import { Component, ElementRef, OnDestroy, OnInit, ViewChild } from "@angular/core";
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ReplaySubject } from "rxjs";
import { PhotoUploadService } from '../../services/photo-upload.service';

@Component({
    selector: 'app-home',
    templateUrl: './home.component.html'
})

export class HomeComponent implements OnInit, OnDestroy {
    photoForm!: FormGroup ;
    private destroyer$ = new ReplaySubject<void>(1)
    constructor(private photoUploadService: PhotoUploadService, 
        private fb: FormBuilder,
        ){}

    createForm(){
        this.photoForm = this.fb.group({
            photo_name: [null, Validators.required]
        })
    }    

    onUploadFile(event: any){
        const files = event.target?.files
        const photo = files[0];
        let formData = new FormData();
        formData.append('image', photo);
        this.photoUploadService.checkPhotoType(formData).subscribe((data)=>{
            console.log(data)
        })
        

    }

    // photoTypeChecker(file: any): Observable<boolean>{
    //     // Reading photo
    //     let photoReader: FileReader = new FileReader();
    //     const subscribe = new Subject<boolean>();
    //     let isValidHeader: boolean;
    //     let header: string = "";
    //     // Reading and Checking the MIME type of the photo
    //     photoReader.onloadend = function(e: any) {
    //       const arr = (new Uint8Array(e.target.result)).subarray(0, 4);
    //       for(let i = 0; i < arr.length; i++) {
    //         header += arr[i].toString(16);
    //       }
    //       if(header === ('ffd8ffe0' || 'ffd8ffe1' || 'ffd8ffe2' || 'ffd8ffe3' || 'ffd8ffe8')){
    //         subscribe.next(true);
    //         subscribe.complete();
    //       }
    //       else{
    //         subscribe.next(false);
    //         subscribe.complete();
    //       }
    //     };
    //     photoReader.readAsArrayBuffer(file);
    //     return subscribe.asObservable();
    //   }
    
    ngOnInit(): void{
        this.createForm();
    }

    ngOnDestroy(): void {
        this.destroyer$.next();
        this.destroyer$.complete();
    }

}